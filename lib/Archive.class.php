<?php

class Archive {
	
	/**
	 * @var Archive Instancja klasy Archive
	 */
	private static $instance;
	
	/**
	 * @var string Ścieżka pliku źródłowego z danymi archiwalnymi
	 */
	private $sourcePath = 'http://www.mbnet.com.pl/dl.txt';
	
	/**
	 * @var array Przetworzone dane archiwalne
	 */
	private $data = [];
	
	/**
	 * Zwraca instancję archiwum danych losowań Dużego Lotka
	 * 
	 * @return Archive
	 */
	public static function instance(){
		if(!self::$instance){
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * 
	 */
	private function __construct(){
		$this->prepareData();
	}
	
	/**
	 * Pobiera i przygotowuje dane archiwalne
	 */
	protected function prepareData(){
		$sourceData = file($this->sourcePath, FILE_IGNORE_NEW_LINES);
		foreach($sourceData as $row){
			$data = explode(' ', $row);
			$this->data[$data[2]][] = $data[1];
		}
	}
	
	/**
	 * Zwraca daty losowania podanej kombinacji liczb
	 * Liczby muszą być uporządkowane w kolejności rosnącej
	 * 
	 * @result string  Kombinacja liczb
	 * @return array
	 */
	public function getByResult($result){
		if(isset($this->data[$result])){
			return $this->data[$result];
		}
		return false;
	}
	
}