<?php


class Lotto {
	
	private $maxNumber = 49;
	
	private $length = 6;
	
	
	public function generateRandomNumbers(){
		$generatedData = [];
		while(sizeof($generatedData) < $this->length){
			$number = rand(1, $this->maxNumber);
			if(!in_array($number, $generatedData)){
				$generatedData[] = $number;
			}
		}
		sort($generatedData);
		return implode(',', $generatedData);
	}
	
	
	public function checkInArchive($numbers){
		include_once 'Archive.class.php';
		if(is_array($numbers)){
			$numbers = implode(',', $numbers);
		}
		$archive = Archive::instance();
		return $archive->getByResult($numbers);
	}
	
}